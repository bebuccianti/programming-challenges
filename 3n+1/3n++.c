#include <stdio.h>
#include <string.h>

int cycle(int n)
{
    int count = 1;

    while (n != 1)
    {
        if (n % 2)
            n = 3 * n + 1;
        else
            n /= 2;

        ++count;
    }

    return count;
}

int maxOfCycles(int a, int b)
{
    int max = 0, temp;

    do
    {
        temp = cycle(a);

        if (temp > max)
            max = temp;
        
    } while (++a <= b);

    return max;
}

int main(void)
{
    int a, b, swap, a0, b0;

    while (scanf("%d %d", &a, &b) != EOF)
    {
        a0 = a;
        b0 = b;

        if (a > b)
        {
            swap = a;
            a = b;
            b = swap;
        }
            
        printf("%d %d %d\n", a0, b0, maxOfCycles(a, b));
    }

    return 0;
}

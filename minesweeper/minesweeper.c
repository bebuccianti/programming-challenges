#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    int x, y, count = 1, i, j, row, col;

    while (scanf("%d%d\n", &x, &y) > 0)
    {
        if (x == 0 && y == 0) continue;

        char field[x][y];
        memset(field, '0', x*y*sizeof(field[0][0]));

        for (i = 0; i < x; i++)
        {
            char line[y+2];
            fgets(line, y+2, stdin);

            for (j = 0; j < y+1; j++)
            {
                if (line[j] == '*')
                {
                    field[i][j] = '*';
                    
                    for (row = i - 1; row <= i + 1; row++)
                    {
                        if (row >= 0 && row < x)
                        {
                            char *looprow = field[row];
                            for (col = j - 1; col <= j + 1; col++)
                                if (col >= 0 && col < y && looprow[col] != '*')
                                    looprow[col]++;
                        }
                    }
                }
            }
        }

        if (x > 0 && y > 0 && count == 1) 
            printf("Field #%d:\n", count);
        else if  (x > 0 && y > 0)
            printf("\nField #%d:\n", count);


        for (i = 0; i < x; i++)
        {
            printf("%.*s\n", y, field[i]);
        }


        count++;
    }
    
    return 0;
}

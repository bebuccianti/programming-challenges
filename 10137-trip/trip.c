#include <stdio.h>

int main(void)
{
    int n, i;
    double avg, pos, neg;
    
    while (scanf("%d\n", &n) && n != 0)
    {
        double amount[n];
        
        avg = 0.0;

        for (i = 0; i < n; i++)
        {
            scanf("%lf\n", &amount[i]);
            avg += amount[i];
        }

        avg /= n;
        pos = neg = 0.0;
        for (i = 0; i < n; i++)
        {
            double value = (long) ((amount[i] - avg) * 100);
            if (value > 0)
                pos += value;
            else
                neg += value;
        }

        neg = -neg;

        printf("$%.2lf\n", neg > pos ? neg/100.0 : pos/100.0);
    }

    return 0;
}
